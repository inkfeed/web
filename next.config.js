const { parsed: localEnv } = require('dotenv').config()
const webpack = require('webpack')
const withCSS = require('@zeit/next-css')
const withSass = require('@zeit/next-sass')

let config = {
  webpack: config => {
    // Fixes npm packages that depend on `fs` module
    config.node = {
      fs: 'empty'
    }

    config.plugins.push(new webpack.EnvironmentPlugin(localEnv ? localEnv : {}))

    return config
  },
}

config = withCSS(config)
config = withSass(config)

module.exports = config;