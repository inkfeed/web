import React from 'react'
import {inject, observer} from 'mobx-react';
import Link from 'next/link'
import Page from "~/layouts/Page";
import './index.scss';
import sample from 'lodash/sample';
import ErrorMessageList from "../../components/ErrorMessageList";
import Head from "../../layouts/Head";

@inject('auth')
@observer
class LoginForm extends React.Component {
    state = {
        username: '',
        password: '',
    }

    onSubmit = (e) => {
        e.preventDefault();
        this.props.auth.login(this.state.username, this.state.password)
    }


    render() {
        if (this.props.auth.isLoggedIn) {
            return <div>
            <Link href="/">Home</Link> loggedin <button onClick={this.props.auth.logout}>logout</button></div>
        }

        return (
            <div>
                {this.props.auth.errorMessages &&
                    <ErrorMessageList fieldErrors={this.props.auth.errorMessages} />
                }
                <form onSubmit={this.onSubmit}>
                    <div className="field">
                        <label className="label">Username</label>
                        <div className="control">
                            <input
                                value={this.state.username}
                                onChange={e => this.setState({ username: e.target.value })}
                                name="username"
                                className="input"
                                placeholder="Username" />
                        </div>
                    </div>

                    <div className="field">
                        <label className="label">Password</label>
                        <div className="control">
                            <input
                                value={this.state.password}
                                onChange={e => this.setState({ password: e.target.value })}
                                name="password"
                                type="password"
                                className="input"
                                placeholder="Password" />
                        </div>
                    </div>
                    <hr />

                    <div className="level">
                        <div className="level-left">
                            <small><a href="{% url 'password_reset' %}">Forgot?</a></small>
                        </div>
                        <div className="level-right">
                            <div className="field">
                                <div className="control">
                                    <button
                                        onClick={() => this.props.auth.login(this.state.username, this.state.password)}
                                        type="submit"
                                        loading={this.props.isLoading}
                                        className="button is-primary is-rounded">
                                        Login
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        )
    }
}

function getClass() {
    const classes = [
        'login-page-1',
        'login-page-2',
        'login-page-3',
    ]
    return sample(classes)
}

const Index = () => (
  <Page className={getClass()}>
      <Head title={"Login"} />
      <div className="is-hv-centered">
          <div className="column is-3">
              <h1 className="title is-3 is-pretty has-text-white">
                  Welcome back.
              </h1>
              <div className="box">
                  <LoginForm />
              </div>
          </div>
      </div>
  </Page>
)

export default Index
