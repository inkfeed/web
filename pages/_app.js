import ErrorPage from 'next/error';
import {configure} from 'mobx';
import App, { Container } from 'next/app'
import {Provider, useStaticRendering} from 'mobx-react';
import axios from 'axios';
import { isServer } from '~/config';
import config from '~/stores';
import MobxApp, {configureMobx} from '~/vendor/MobxApp';
import { rehydrate } from '~/vendor/persistence';
import '~/vendor/fa';
import '~/styles/index.scss';
import Head from "../layouts/Head";

configure({enforceActions: 'observed'});
useStaticRendering(isServer);

axios.defaults.baseURL = process.env.API_URL ? process.env.API_URL : 'https://inkfeed.mattei.dev';

class Inkfeed extends MobxApp {
  static async getInitialProps(props) {
    return this.init(props)
  }

  componentDidMount() {
      rehydrate()
  }

  render() {
    const {Component, pageProps, store} = this.props;
    const {statusCode} = pageProps;

    if (statusCode && statusCode >= 400) {
      return <ErrorPage statusCode={statusCode} />;
    }

    return (
      <Container>
        <Head />
        <Provider {...store}>
          <Component {...pageProps} />
        </Provider>
      </Container>
    );
  }
}


export default configureMobx(config, Inkfeed);