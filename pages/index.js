import React from 'react'
import Link from 'next/link'
import Head from '../layouts/Head'
import {inject, observer} from 'mobx-react';
import Page from "../layouts/Page";
import {getLatestIdeas} from "../lib/ideas";
import Idea from "../components/Idea";

const Stream = ({ ideas }) => {
  return ideas.map(idea => <Idea idea={idea} key={idea.id} />)
}

@inject('auth')
@observer
class Home extends React.Component {
    static async getInitialProps({ req }) {
        try {
            const data = await getLatestIdeas();
            return {
                next: data.next,
                ideas: data.results
            }
        } catch (e) {
            return {
                failed: true,
            }
        }
    }

    render() {
        if (this.props.failed) return <Page>Uh oh. Shit went wrong.</Page>

      return (
        <Page>
            <Stream ideas={this.props.ideas} />
        </Page>
      )
    }
}

export default Home
