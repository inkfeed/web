import React from 'react';
import {Button, Card, Control, Field, Icon, Input, Message, Subtitle, Title} from "~/vendor/bulma";
import axios from 'axios';
import Emoji from "~/components/Emoji";
import { validateEmail } from '~/lib/validators';
import Spinner from "~/components/Spinner";
import ReCaptcha from 'react-google-recaptcha';
import FontAwesomeIcon from "@fortawesome/react-fontawesome";
import Page from "../../layouts/Page";
import './index.scss';
import {Box} from "../../vendor/bulma";
import ErrorMessageList from "../../components/ErrorMessageList";
import {inject, observer} from 'mobx-react';
import { isServer } from '~/config';
import redirect from 'next-redirect';
import Head from "../../layouts/Head";

class AccountActivator extends React.Component {
    state = {
        activating: true,
        failed: false,
    }

    componentDidMount() {
        if (this.props.uid && this.props.token) {
            this.activate()
        }
    }

    activate = async () => {
        try {
            this.setState({activating: true,})
            const response = await axios.get(`/accounts/activate/${this.props.uid}/${this.props.token}/`);
            console.log("Activated", response.data)
            redirect({}, '/')
            if (this.props.onConfirmEmail) {
                this.props.onConfirmEmail(response.data.token)
            }
        } catch (e) {
            this.setState({failed: true, activating: false,})
        }
    }

    render() {
        return (
            <>
                {!this.state.failed && this.state.activating && <center><Spinner text={"Activating your account..."} /></center>}
                {this.state.failed &&
                <Message danger>
                    <Message.Body>
                        Could not activate your account. Message us for more information.
                    </Message.Body>
                </Message>
                }
            </>
        )
    }
}

class RegisterForm extends React.Component {
    state = {
        registered: false,
        email: '',
        emailPrefilled: false,
        code: '',
        username: '',
        password: '',
        showPassword: false,
        repeat_password: '',
        recaptchaToken: '',
        showAdvanced: false,
        isRegistering: false,
        errorMessages: null,
    }

    componentDidMount() {
        this.captcha.execute()
    }

    arePasswordsEqual = () => {
        return this.state.password === this.state.repeat_password
    }

    toggleShowPassword = () => {
        this.setState({
            showPassword: !this.state.showPassword
        })
    }

    isFormValid = () => {
        if (!this.arePasswordsEqual()) {
            return false
        }

        return true
    }

    submitForm = async () => {
        this.setState({ isRegistering: true })
        const data = {
            email: this.state.email,
            invite_code: this.state.code,
            username: this.state.username,
            password: this.state.password,
            repeat_password: this.state.repeat_password,
            recaptcha_token: this.state.recaptchaToken,
        }

        try {
            // also try logging user in right away
            const response = await axios.post('/accounts/register/', data)
            if (response.data.success) {
                this.setState({
                    registered: true
                })
            }
        } catch (e) {
            if (e.response && e.response.data && e.response.status === 400) {
                this.setState({
                    registered: false,
                    isRegistering: false,
                    errorMessages: e.response.data
                })
            }
        }
    }

    verifyCallback = (recaptchaToken) => {
        // Here you will get the final recaptchaToken!!!
        this.setState({
            recaptchaToken: recaptchaToken
        })
    }

    // auto check available username

    renderForm = () => (
        <div>
            {this.state.errorMessages !== null && this.state.errorMessages.non_field_errors !== null &&
                <ErrorMessageList fieldErrors={this.state.errorMessages} />
            }
            <Field>
                <label className="label">Username</label>
                <Control className="control has-icons-left">
                    <Icon medium className={"is-left"}>
                        @
                    </Icon>
                    <Input
                        value={this.state.username}
                        onChange={(e) => this.setState({ username: e.target.value.toLowerCase() })}
                        danger={this.state.errorMessages && this.state.errorMessages.username}
                        placeholder="Username" />
                </Control>
                <p className="help">
                    {this.state.errorMessages && this.state.errorMessages.username ?
                        this.state.errorMessages.username
                        :
                        'Pick a username. Be creative!'
                    }
                </p>
            </Field>
            {!this.state.emailPrefilled &&
            <Field>
                <label className="label">Email address</label>
                <Control>
                    <Input
                        danger={this.state.errorMessages && this.state.errorMessages.email}
                        value={this.state.email}
                        onChange={(e) => this.setState({ email: e.target.value })}
                        placeholder="Email"
                    />
                </Control>
                {this.state.errorMessages && this.state.errorMessages.email ?
                    <p className="help">{this.state.errorMessages.email}</p>
                    :
                    <p className="help">I won't spam you, I promise!</p>
                }
            </Field>
            }
            <Field>
                <label className="label">Password</label>
                <Control>
                    <Input
                        value={this.state.password}
                        onChange={(e) => this.setState({ password: e.target.value, repeat_password: e.target.value })}
                        type={this.state.showPassword ? null : 'password'}
                        danger={this.state.errorMessages && this.state.errorMessages.password}
                        placeholder="Password" />
                    <p className="help">
                        {
                            // eslint-disable-next-line
                        } <a onClick={this.toggleShowPassword}>{this.state.showPassword ? 'Hide password': 'Show password'}</a>
                    </p>
                </Control>
                {this.state.errorMessages && this.state.errorMessages.password ?
                    <p className="help">{this.state.errorMessages.password}</p>
                    :
                    null
                }
            </Field>

            <hr />

            <center>
                <Button
                    className={'is-rounded'}
                    disabled={!this.isFormValid()}
                    medium primary
                    loading={this.state.isRegistering}
                    onClick={this.submitForm}>
                    Join Inkfeed
                </Button>
            </center>
        </div>
    )

    renderFinishedStep = () => (
        <center>
            <Subtitle>The activation email is on the way! <Emoji emoji={"💌"} /></Subtitle>
            <p className={"help"}>It was sent to {this.state.email}. <br /> If it takes too long to arrive, don't hesitate to tweet at @matteing.</p>
        </center>
    )

    render() {
        return (
            <>
                {!this.state.registered && this.renderForm()}
                {this.state.registered && this.renderFinishedStep()}

                <ReCaptcha
                    ref={(el) => { this.captcha = el; }}
                    size={"invisible"}
                    sitekey="6LfiJoQUAAAAAD-OhK2h2iy8cgnx3Qk5s9kqeLmb"
                    onChange={this.verifyCallback}
                />
            </>
        )
    }
}

@inject('auth')
@observer
class RegisterPage extends React.Component {
    state = {
        inviteRequested: false,
        isSubmitting: false,
        bio: '',
        email: '',
        errorMessages: null
    }

    static getInitialProps({query}) {
        return {query}
    }

    isConfirming = () => {
        let params = this.getQuery()
        return params.uid && params.token
    }

    getQuery = () => {
        if (!isServer) {
            const urlParams = new URLSearchParams(window.location.search);
            return {
                uid: urlParams.get('uid') ? urlParams.get('uid') : false,
                token: urlParams.get('token') ? urlParams.get('token') : false,
            }
        } else {
            return {
                uid: this.props.query.uid ? this.props.query.uid : null,
                token: this.props.query.token ? this.props.query.token : null,
            }
        }
    }

    isValidEmail = () => {
        if (this.state.email.length === 0) {
            return false;
        }

        if (!validateEmail(this.state.email)) {
            return false;
        }

        return true;
    }

    isValidBio = () => {
        // not obligatory for now.
        return true;
    }

    submitRequest = async () => {
        const data = {
            'email': this.state.email,
            'bio': this.state.bio,
        }

        try {
            this.setState({isSubmitting: true})
            await axios.post('/invites/request/', data);
            this.setState({inviteRequested: true})
        } catch (e) {
            if (e.response && e.response.data && e.response.status === 400) {
                this.setState({
                    isSubmitting: false,
                    errorMessages: e.response.data
                })
            }
        }
    }

    render() {
        return (
            <Page className={"register-page"}>
                <Head title={"Join"} />
                <div className="is-hv-centered">
                    <div className="column is-4">
                        <Title is={"3"} className={"is-pretty has-text-white"}>
                            Let's get started!
                        </Title>
                        <Subtitle className={"has-text-white"}>
                            You're three steps away from greatness.
                        </Subtitle>

                        <Box style={{minWidth: 400}}>
                            { this.isConfirming() ?
                                <AccountActivator
                                    uid={this.getQuery().uid}
                                    token={this.getQuery().token}
                                    onConfirmEmail={t => this.props.auth.login('', '', t)} />
                                : <RegisterForm  />}
                        </Box>
                    </div>
                </div>
            </Page>
        )
    }
}

export default RegisterPage;
