import React from 'react';

export default props => (
    <footer className="hero is-light is-small">
        <div className="hero-body">
            <div className="container">
                <div className="level">
                    <div className="level-left">
                        <div className="level-item">
                            <img className="logo" style={{maxHeight: 20}} src="https://sergiomattei.com/img/Arkus-Fatter-Black.png"/>
                        </div>
                    </div>


                    <div className="level-right">
                        <div className="level-item">
                            <a href="#">About</a>
                        </div>
                        <div className="level-item">
                        </div>

                        <div className="level-item">
                            <a href="#">Maker</a>
                        </div>
                        <div className="level-item">
                        </div>
                        <div className="level-item">
                            <a href="#">Twitter</a>
                        </div>
                        <div className="level-item">
                        </div>
                        <div className="level-item">
                            <a href="#">Legal</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
)