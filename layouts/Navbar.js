import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import format from "date-fns/format";
import {inject, observer} from "mobx-react/index";
import Link from 'next/link';
import Emoji from "../components/Emoji";
import ActiveLink from "../components/ActiveLink";
import PostModal from '~/components/PostModal';
import {isServer} from "../config";

function getDate() {
  const now = new Date();
  return format(now, "MMMM D, YYYY");
}

class TopicNav extends React.Component {
  render() {
    return (
      <div className="topic-nav">
        <nav className="container level is-mobile">
          <a
            className="level-item has-text-centered topic-link active heading"
            href="index.html"
          >
            <span className="icon is-small">
              <FontAwesomeIcon icon={"fire"} />
            </span>{" "}
            Top stories
          </a>
          <a className="level-item has-text-centered topic-link heading">
            <span className="icon is-small">
              <FontAwesomeIcon icon={"newspaper"} />
            </span>
            &nbsp;Latest
          </a>
          <a
            className="level-item has-text-centered topic-link heading"
            href="ask.html"
          >
            Ask <span className="unread-dot" />
          </a>
          <a className="level-item has-text-centered topic-link heading">
            News
          </a>
          <a className="level-item has-text-centered topic-link heading">
            Makers
          </a>
          <a className="level-item has-text-centered topic-link heading">
            Science & Tech
          </a>
          <a className="level-item has-text-centered topic-link heading">
            Programming
          </a>
          <a className="level-item has-text-centered topic-link heading">
            Ideas
          </a>
          <a className="level-item has-text-centered topic-link heading">
            Lifestyle
          </a>
        </nav>
      </div>
    );
  }
}

@inject('auth')
@observer
class Navbar extends React.Component {
  render() {
    return (
      <>
        <nav className="navbar" role="navigation" aria-label="main navigation">
          <div className="container">
            <div className="navbar-brand">
              <Link href={"/"}>
                  <a className="navbar-item is-logo">
                      <img src="/static/logo.png" />
                  </a>
              </Link>
            </div>

            <div id="navbarBasicExample" className="navbar-menu">
              <div className="navbar-start">
                <a className="navbar-item date-item" href={""}>
                  <strong>{getDate()}</strong>
                </a>
              </div>
                <div className={"navbar-end"}>
                {!this.props.auth.isLoggedIn &&
                    <>
                      <div className={"navbar-item"}>
                          <Link href={'/begin'}>
                              <a className="button is-primary is-rounded">
                                  <strong>Join Inkfeed</strong>
                              </a>
                          </Link>
                        </div>
                        <ActiveLink activeClassName={"is-active"} href={'/login'}>
                            <a className="navbar-item">
                                <strong>Login</strong>
                            </a>
                        </ActiveLink>
                    </>
                }
                  {this.props.auth.isLoggedIn && this.props.auth.user &&
                    <>
                        <div className={"navbar-item"}>
                            <button onClick={e => this.animated.show()} className="button is-primary is-rounded">
                                <strong>New post</strong>
                            </button>
                            {!isServer && <PostModal setRef={ref => this.animated = ref} />}
                        </div>
                        <ActiveLink activeClassName={"is-active"} href={`/@${this.props.auth.user.username}`}>
                            <a className="navbar-item">
                                <FontAwesomeIcon icon={'bell'} />
                            </a>
                        </ActiveLink>
                        <ActiveLink activeClassName={"is-active"} href={`/@${this.props.auth.user.username}`}>
                            <a className="navbar-item">
                                <FontAwesomeIcon icon={'bookmark'} />
                            </a>
                        </ActiveLink>
                        <ActiveLink activeClassName={"is-active"} href={`/@${this.props.auth.user.username}`}>
                            <a className="navbar-item">
                                <img className="is-circle" src={this.props.auth.user.avatar} />
                            </a>
                        </ActiveLink>
                    </>
                    }
                </div>
            </div>
          </div>
        </nav>
        <TopicNav />
      </>
    );
  }
}

export default Navbar;
