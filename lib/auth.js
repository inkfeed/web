import axios from 'axios';
import {prettyAxiosError} from "./utils/error";

export async function login(username, password) {
    try {
        const payload = { username: username, password: password }
        const response = await axios.post('/api-token-auth/', payload);
        return response.data['token'];
    } catch (e) {
        // return a pretty error
        prettyAxiosError(e)
    }
}

export async function getUser() {
    const endpoint = 'me';
    const { data } = await axios.get(endpoint);
    return data
}