import axios from 'axios';
import {prettyAxiosError} from "./utils/error";

export async function getLatestIdeas(offset=0) {
    try {
        const response = await axios.get(`/ideas/`);
        return response.data;
    } catch (e) {
        // return a pretty error
        prettyAxiosError(e)
    }
}


export async function getUrlMetadata(url) {
    try {
        const response = await axios.post(`/ideas/metadata/`, { url });
        return response.data;
    } catch (e) {
        // return a pretty error
        prettyAxiosError(e)
    }
}