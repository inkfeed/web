import { create } from 'mobx-persist';
import { isServer } from '~/config';
import storeConfig from '../stores';

// No arguments (arguments are where to store like localForage.)
export const hydrate = create()

export function rehydrate(config=storeConfig) {
    if (!isServer && config.stores) {
        console.log("Persist: Rehydrating stores...")
        Object.keys(config.stores).map(
            k => {
                if (config.persist.includes(k)) {
                    console.log("Persist: Rehydrating", k, config.stores[k]())
                    hydrate(k, config.stores[k]())
                }
            }
        )
    }
}