import {withMobx} from 'next-mobx-wrapper';
import App from 'next/app'
import { isServer } from '~/config';
import {onStoreInit} from '~/stores';

export function configureMobx(config, app) {
    const getStores = config.stores;

    return withMobx(getStores)(app);
}

class MobxApp extends App {
  static async init({Component, ctx, ...props}) {
    let pageProps = {};

    if (typeof Component.getInitialProps === 'function') {
      pageProps = await Component.getInitialProps(ctx);
    }

    if (onStoreInit && isServer) {
        // Loads onServerLoad - this is the place to put your cookie things.
        await onStoreInit(ctx)
    }

    return {pageProps};
  }
}

export default MobxApp;