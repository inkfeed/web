import { library } from '@fortawesome/fontawesome-svg-core'
import {faFire} from "@fortawesome/free-solid-svg-icons/faFire";
import {faNewspaper} from "@fortawesome/free-solid-svg-icons/faNewspaper";
import {faHandPeace} from "@fortawesome/free-solid-svg-icons/faHandPeace";
import {faBookmark} from "@fortawesome/free-solid-svg-icons/faBookmark";
import {faBell} from "@fortawesome/free-solid-svg-icons/faBell";

library.add(
    faFire,
    faNewspaper,
    faHandPeace,
    faBookmark,
    faBell,
)