import React from 'react';
import PropTypes from 'prop-types';
import CompactIdea from "./CompactIdea";

class Idea extends React.Component {
    render() {
        return (
            <CompactIdea idea={this.props.idea} />
        )
    }
}

export default Idea;