import React from 'react';
import PropTypes from 'prop-types';
import './CompactIdea.scss';
import OutboundLink from "../OutboundLink";

class CompactIdea extends React.Component {
    shortenSummary = (summary) => {
        const regex = /.*?(\.)(?=\s[A-Z])/;
        let m;

        if ((m = regex.exec(summary)) !== null) {
            return m[0]
        } else {
            return summary
        }
    }

    render() {
        if (!this.props.idea) return null;

        if (this.props.idea.kind === 'text') {
            return null
        }

        return (
            <article className="CompactIdea media">
                {this.props.idea.image &&
                    <figure className="media-left">
                        <OutboundLink to={this.props.idea.link}>
                                <p className="image is-3by2">
                                    <img src={this.props.idea.image} />
                                </p>
                        </OutboundLink>
                    </figure>
                }
                <div className="media-content">
                    <OutboundLink to={this.props.idea.link}>
                        <h1 className="title is-6 article-title">
                            {this.props.idea.title}
                        </h1>
                        {this.props.idea.summary &&
                        <p className="summary">
                            {this.shortenSummary(this.props.idea.summary)}
                        </p>
                        }
                    </OutboundLink>
                    <nav className="level is-mobile">
                        <div className="level-left">
                            <div className="level-item">
                            <span className="heading">
                                <strong>2.5k comments</strong>
                            </span>
                            </div>
                            <div className="level-item">
                            <span className="heading">
                                <strong>25 ideas</strong>
                            </span>
                            </div>
                            <div className="level-item">
                            <span className="heading">
                                80% / 20%
                            </span>
                            </div>
                        </div>
                    </nav>
                </div>
            </article>
        )
    }
}

export default CompactIdea;