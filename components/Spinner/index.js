import React from 'react';
import './index.scss';

export default props => {
    if (props.large) {
        return (
            <div className={"Spinner large"}>
                <img src={"/static/logo.png"} />
            </div>
        )
    } else {
        return (
            <div className="Spinner small">
                <div className="sk-cube1 sk-cube"></div>
                <div className="sk-cube2 sk-cube"></div>
                <div className="sk-cube4 sk-cube"></div>
                <div className="sk-cube3 sk-cube"></div>
            </div>
        )
    }
}