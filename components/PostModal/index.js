import React from 'react';
import SkyLight from 'react-skylight';
import './index.scss';
import PostWizard from "./PostWizard";

const myBigGreenDialog = {
    color: '#ffffff',
    borderRadius: 5,
};


export default props => (
    <SkyLight
        dialogStyles={myBigGreenDialog}
        hideOnOverlayClicked
        ref={props.setRef}
        transitionDuration={300}
    >
        <PostWizard />
    </SkyLight>
);