import React from 'react';
import Emoji from "../Emoji";
import './PostWizard.scss';
import {Step, Steps, Wizard} from "react-albus";
import debounce from 'lodash/debounce';
import {getUrlMetadata} from "../../lib/ideas";
import Idea from "../Idea";

function validURL(str) {
    var pattern = new RegExp('^(https?:\\/\\/)?'+ // protocol
        '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|'+ // domain name
        '((\\d{1,3}\\.){3}\\d{1,3}))'+ // OR ip (v4) address
        '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ // port and path
        '(\\?[;&a-z\\d%_.~+=-]*)?'+ // query string
        '(\\#[-a-z\\d_]*)?$','i'); // fragment locator
    return !!pattern.test(str);
}

const StepOne = props => (
    <div className={"PostWizard step-one columns"}>
        <div className={"column"} onClick={e => props.onSelect('link', props.next)}>
            <div>
                <Emoji emoji={"🔗"} />
            </div>
            <div className={"title is-4"}>
                Share a link
            </div>
        </div>
        <div className={"column"} onClick={e => props.onSelect('text', props.next)}>
            <div>
                <Emoji emoji={"✍️"} />
            </div>
            <div className={"title is-4"}>
                Share an idea
            </div>
        </div>
        <div className={"column"} onClick={e => props.onSelect('question', props.next)}>
            <div>
                <Emoji emoji={"🤔"} />
            </div>
            <div className={"title is-4"}>
                Ask a question
            </div>
        </div>
    </div>
)

class LinkWizard extends React.Component {
    state = {
        url: '',
        loading: false,
        advanced: false,
        summary: '',
        image: '',
    }

    setUrl = url => {
        let newUrl = url;

        if (url.length !== 0 && !url.startsWith('http://') && !url.startsWith('https://')) {
            newUrl = `https://${url}`
        }
        this.setState({
            url: newUrl.toLowerCase()
        })

        this.getMetadata()
    }

    getMetadata = debounce(async () => {
        if (validURL(this.state.url)) {
            this.setState({
                loading: true
            })
            try {
                const { title, summary, image } = await getUrlMetadata(this.state.url);
                this.setState({
                    title, summary, image,
                    loading: false
                })
            } catch (e) {
                this.setState({
                    loading: false
                })
            }
        }
    })


    render() {
        return (
            <div className={"PostWizard step-link"}>
                <div className="field">
                    <div className={"control is-large" + (this.state.loading ? ' is-loading' : '')}>
                        <input
                            value={this.state.url}
                            onChange={e => this.setUrl(e.target.value)}
                            className="input is-large is-rounded"
                            type="text"
                            placeholder="What's the link?" />
                    </div>
                </div>
                    {this.state.title && this.state.summary && this.state.image &&
                        <div className={"idea-case"}>
                            <Idea idea={{title: this.state.title, summary: this.state.summary, image: this.state.image}} />
                        </div>
                    }
            </div>
        )
    }
}

const TextWizard = props => (
    <div></div>
)

class PostWizard extends React.Component {
    state = {
        'type': null,
    }

    onClickType = (type, next) => {
        this.setState({ type })
        next()
    }

    renderTypeWizard = (previous, next) => {
        switch (this.state.type) {
            case 'link':
                return <LinkWizard previous={previous} next={next} />
                break;

            case 'text':
                return <TextWizard previous={previous} next={next} />
        }
    }

    render() {
        return (
            <Wizard>
                <Steps>
                    <Step
                        id="one"
                        render={({ next }) => <StepOne onSelect={this.onClickType} next={next} />}
                    />
                    <Step
                        id="two"
                        render={({ previous, next }) => this.renderTypeWizard(previous, next)}
                    />
                </Steps>
            </Wizard>
        )
    }
}

export default PostWizard;