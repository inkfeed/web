import { observable, computed, action, runInAction, flow, when, reaction } from 'mobx'
import { persist } from 'mobx-persist'
import { login, getUser } from '../lib/auth';
import axios from 'axios';
import {BaseStore, getOrCreateStore} from 'next-mobx-wrapper';
import setCookie from 'set-cookie';

/*
This is the only state we PERSIST ON THE SERVER, not on the frontend.

Remmeber to super props the component or else it won't have the server data
*/

class AuthStore extends BaseStore {
    @observable isLoading = false;
    @observable token = null;
    @observable user = null;
    @observable error = false;
    @observable errorMessages = null;

    @computed get isLoggedIn() {
        return this.token !== null && this.user !== null;
    }

    constructor(props) {
        super(props);
        when(
            // once...
            () => this.token && this.token !== '' && this.token !== 'null',
            // ... then
            () => this.setTokenHeader(this.token)
        );
    }

    getUser = flow(function* () {
        this.isLoading = true;
        this.user = null;

        try { 
            let user = yield getUser();
            this.user = user;
        } catch (e) {
            this.error = true;
            this.errorMessages = e.message || e.field_errors;
        }
    })

    login = flow(function* (username, password, token=null) {
        this.token = null;
        this.user = null;
        this.isLoading = true;

        try {
            if (token) {
                token = token
            } else {
                token = yield login(username, password);
            }
            this.setToken(token)
            yield this.getUser();
            this.isLoading = false;
        } catch (e) {
            this.isLoading = false;
            this.error = true;
            this.errorMessages = e.message || e.field_errors;
        }
    })

    @action
    setToken = (token, res=null) => {
        this.token = token;
        setCookie('token', token, { res })
        axios.defaults.headers.common['Authorization'] = `Token ${token}`;
    }

    @action
    logout = (res=null) => {
        this.token = null;
        this.user = null;
        setCookie('token', '', { res })
        axios.defaults.headers.common['Authorization'] = '';
    }

    setTokenHeader = (token) => {
        axios.defaults.headers.common['Authorization'] = `Token ${token}`;
    }
}

export const getAuthStore = getOrCreateStore('auth', AuthStore);