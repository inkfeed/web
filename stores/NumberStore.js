import { observable, computed, action } from 'mobx'
import {BaseStore, getOrCreateStore} from 'next-mobx-wrapper';
import setCookie from 'set-cookie';

/*
This is the only state we PERSIST ON THE SERVER, not on the frontend.

Remmeber to super props the component or else it won't have the server data
*/

class NumberStore extends BaseStore {
    @observable number = 0;
    
    @action 
    setNumber = (number) => {
        this.number = number;
    }
}

export const getNumberStore = getOrCreateStore('number', NumberStore);