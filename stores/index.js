import { getNumberStore as number } from './NumberStore';
import { getAuthStore as auth } from './AuthStore';
import cookies from 'next-cookies';
import axios from "axios/index";


export async function onStoreInit(ctx) {
    /**
     * This is where the magic happens. Update your stores here.
     */
    let { token } = cookies(ctx);
    if (token && token !== '' && token !== 'null') {
        ctx.store.auth.setToken(token, ctx.res)
        axios.defaults.headers.common['Authorization'] = `Token ${token}`;
        await ctx.store.auth.getUser()
    }
}


const config = {
    stores: {
        auth
    },
    persist: [
    ],
}


export default config;